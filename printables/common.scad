/* 
 * αφαιρετέο μοντέλο βίδας
 *
 */
 
 
module bolt(
    total_height, // when the bolt is removed this is the total height of the removed part (bolt head is extended upwards). Values smaller than actual bolt height are effectively ignored.
    bolt_coned,  // bolt_coned - έχει φρέζα η βίδα ή όχι
    flange_hole_head_d, // flange_hole_head_d - διάμετρος του κεφαλιού της βίδας (περιλαμβάνει τον αέρα)
    flange_hole_d, // flange_hole_d -  η διάμετρος της τρύπας που περνά το σπείρωμα της βίδας που δένει στον δίσκο. 
    flange_hole_depth // flange_hole_depth - το ύψος του σπειρώματος στη φλάτζα
    ) {
    head_height = bolt_coned ? (flange_hole_head_d - flange_hole_d)/2 : 0;
    translate([0,0,head_height+flange_hole_depth]) {
        translate([0,0,-head_height]) {
            translate([0,0,-flange_hole_depth-0.01]) {
                cylinder(h=flange_hole_depth+0.01,d=flange_hole_d, $fn=30);
            }
            if (bolt_coned)
                cylinder(h=head_height, d1=flange_hole_d, d2=flange_hole_head_d, $fn=30);
        }
        translate([0,0,-0.01]) cylinder(h=total_height-head_height-flange_hole_depth,d=flange_hole_head_d, $fn=30);
    }

}

module flange(
    base_d,
    base_t, // πάχος της βάσης μέχρι πριν αρχίσει ο κώνος
    flange_height, // το ύψος της φλάτζας συνολικά
    top_d,
    //center_d, // διάμετρος της τρύπας στο κέντρο
    hole_count, // πόσες τρύπες για βίδες έχει η φλάτζα
    hole_margin, // περιθώριο των τρυπών από την εξωτερική διάμετρο στη βάση
    hole_head_d,
    hole_depth,
    hole_d,
    bolt_coned
) {
    konos_h = flange_height - base_t; // το ύψος του κωνικού σχηματος (δεν περιλαμβάνει το πάχος της βάσης)
    difference() {
            translate([0,0,flange_height]) {
                    difference() {
                        translate([0,0,-konos_h]) {
                                union() {
                                    cylinder(h=konos_h, d1=base_d, d2=top_d);
                                    // flange
                                    translate([0,0,-base_t]) {
                                        cylinder(h=base_t, d=base_d);
                                    }
                                }
                        }
                        //translate([0,0,-top_inlet_h]) cylinder(h=h_bearing_t+1, d=h_bearing_d_outer); // extend a little for better viewing
                        //translate([0,0,-part_height-1]) cylinder(h=bottom_inlet_h+1,d=h_bearing_d_outer);
                        //translate([0,0,-part_height]) cylinder(h=part_height, d=distancer_outer_d + 2*distancer_margin);
                    }
            }
            for (hole_angle = [0: 360/hole_count : 360])
                    rotate([0,0,hole_angle]) translate([base_d/2-hole_head_d/2-hole_margin,0,0]) 
                        bolt(
                            total_height = flange_height, 
                            bolt_coned = bolt_coned,
                            flange_hole_head_d = hole_head_d, 
                            flange_hole_d = hole_d, 
                            flange_hole_depth = hole_depth);
        }    
}
