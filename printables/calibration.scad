/*

    Printing calibaration part
    
    Tweak values in setup.scad  print this calibration part and see how well hardware parts (bearings and bolts) fit 
    into the 3d-printed parts.
    
*/

//include <setup.scad>
use <common.scad>
//use <rotor.scad>


//prova_hub();
//prova_bolt(prova_offset=0);
//prova_distancer(prova_offset=0, thickness_outside=4);


prova_h= h_bearing_t+5;

module prova_hub() {
    rotate([180,0,0]) translate([0,0,-u_hub_bottom_height]) difference() {
    intersection () {
       hub_bottom();
        translate([0,0,1]) cylinder(d=u_bottom_rim_width*2 + h_bearing_d_outer,h=u_hub_bottom_height);
    }
    cylinder(d=u_bottom_d,h=u_hub_bottom_height-prova_h);
}
}


module prova_bolt(prova_offset){
    translate([(u_bottom_rim_width*2 + h_bearing_d_outer)/2+c_flange_hole_head_d/2*1.5-2+prova_offset,0,0]) difference() {
    cylinder(h=prova_h,d=c_flange_hole_head_d*1.5);
    bolt(prova_h+1,bolt_coned=h_bolt_coned,flange_hole_head_d=c_flange_hole_head_d,flange_hole_d=c_flange_hole_d,flange_hole_depth=h_bearing_t/2);
}
}

module prova_distancer(prova_offset, thickness_outside) {
    distancer_inner_d = h_rod_d + 1.5;
    rotate([0,0,90]) translate([(u_bottom_rim_width*2 + h_bearing_d_outer)/2+(distancer_inner_d+thickness_outside*2)/2-3+prova_offset,0,0])
    difference() {
        cylinder(h=prova_h, d=distancer_inner_d+thickness_outside*2);
        cylinder(h=prova_h, d=distancer_inner_d);        
    }
}


