/*
 * Distancer
 *
 * Keeps the bearings apart in a fixed distance.
 *
 */

//include <setup.scad>

//echo("distancer height: ", distancer_h);

module distancer() {
    distancer_h = u_hub_bottom_height - (u_rod_margin + h_nut_t + h_bearing_t +  h_washer_t + h_washer_t + h_bearing_t) + Distancer_length_tolerance;  
    distancer_inner_d = h_rod_d + distancer_d_inner_tolerance; // αρκετό ώστε να μην βιδώνει η ντίζα τον distancer

    color([0,1,0]) 
    difference() {
        cylinder(h=distancer_h, d=c_distancer_outer_d);
        translate([0,0,-1])
            cylinder(h=distancer_h+1+1, d=distancer_inner_d);        
    }
}

//distancer();