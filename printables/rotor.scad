/*
 *
 * Hub bottom
 *
 * The conic flange that hosts the bearings. The shaft spins inside it.
 *
 */
  
//include <setup.scad>
use <common.scad>


module hub_bottom() {  
    top_inlet_h = c_bearing_t;
    bottom_inlet_h = c_bearing_t + c_nut_t + u_rod_margin;
    
    difference() {
        flange(
            base_d = u_bottom_d,
            base_t = c_flange_t, // base thickness until the start of the cone
            flange_height = u_hub_bottom_height, // total flange height
            top_d = u_bottom_rim_width*2 + c_bearing_d_outer_hole,  // upper side of the cone
            //center_d = c_bearing_d_outer_hole, // center hole diameter
            hole_count = u_hole_count, // number of holes in the flange
            hole_margin = 6, // margin from the edge of the hole until the flange perimeter
            hole_head_d = c_flange_hole_head_d,
            hole_depth = c_flange_hole_depth,
            hole_d = c_flange_hole_d,
            bolt_coned = h_bolt_coned
        );        
        translate([0,0,u_hub_bottom_height]) {
                    translate([0,0,-top_inlet_h]) cylinder(h=c_bearing_t+1, d=c_bearing_d_outer_hole); // extend a little for better viewing
                    translate([0,0,-u_hub_bottom_height-1]) cylinder(h=bottom_inlet_h+1,d=c_bearing_d_outer_hole);
                    translate([0,0,-u_hub_bottom_height]) cylinder(h=u_hub_bottom_height, d=c_distancer_outer_d + 2*u_distancer_margin);
        }
    }
}


//echo("rotor height: ", u_hub_bottom_height);
//hub_bottom();

