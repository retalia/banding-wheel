# Bill of Materials

| Item No. | Part Name          | Description                    | Quantity | Cost (Estimated) |
|----------|--------------------|--------------------------------|----------|------------------|
| 1        | Bearing            | 6000ZZ (10mm Bore, 26mm  OD)   | 2        | 1.6 €            |
| 2        | Threaded Rod       | M10, ~100 mm length            | 1        | 1 €              |
| 3        | Hex Nuts           | M10                            | 4        | 0.5 € 		   |
| 4        | Washer (small)     | M10, 20mm OD  		         | 2        | 0.2 €			   |
| 5        | Washer (large)     | M10, >20mm OD                  | 1        | 0.1 €			   |
| 6        | Plywood            | ~ 220 X 220 mm, width 10-20mm	 | 2        | 5 €			   |
| 7        | Wood screws        | ~ 20mm length             	 | 10       | 1 €			   |
| 8        | PETG Filament      | 140 gr				         | 1        | 4 €			   |


## Notes:
* This Bill of Materials is designed for the M10 system and the default print files
* Small washers (Item No. 4) are suggested of ~ 2.15mm width, and OD (Outer Diameter) should be smaller than the bearing
* Symbol _~_ means precision is not needed
* Refer to the Documentation for using hardware with different specifications.