# Versa Wheel

<img src="photos/banding wheel.jpg" width = "500"> 

Instructions to make, put together and tweak a banding wheel for ceramics. Documentation, photos, source code and development of this project is carried out on [repository](https://gitlab.com/retalia/versa-wheel/). The project was first developed by [Retalia Community](http://retalia.org).

# Table Of Contents

[TOC]


# Before you start - materials, tools, time and purpose

## Bill of Materials

Before you start make sure you have the materials as they are described in [Bill of materials](../Bill of materials.md). We adviced to choose the 10mm system for hardware parts (threaded rod, nuts and bearings). Using bigger hardware and 3d printed parts is also possible and supported by the source .scad files in the repository.

<img src="photos/bill of materials.jpg" width = "300"> 

Note: If the two small washers have thickness other than the recommended (2.15mm), refer to the Appendix (where you will find detailed information 3d printed parts customization). Tolerances up to 0.5mm are ok.

## Tools

Along with materials you will need the following tools:

* two 17 mmwrenches to tighten M10 nuts
* a screwdriver for screws on the wooden sheets
* drill
* c-clamp for testing, sawing and working
* a marker pen for marking cutting lines for accurate sawing
* a handsaw or Jigsaw
* a corse sandpaper for the final touch

For 3d printing you will need basic skills (printing 'stl' files)

## Time

Having a complete set of 3d printed and hardware parts in place you will need approximately 60 minutes to assemble and 30 more minutes to cut the wood. That is, 90 minutes assembly time in total.

3d printing can take some boring time to complete - actually it takes 4 to 6 hours -  and waste your material if the hardware parts don't fit nicely in the printed ones. To address this, a calibration part exists. It's a small printed piece that should be ready in 15-30 minutes and helps you verify your hardware before moving forward with the actual 3d printed parts. This can be an opportunity for you to skim through the rest of this document and roll up your sleeves for the assembly.

For hardware modifications (see Appendix), estimate a rough of 30-90 minutes in iterations. Measuring hardware, modifing `calibration` part and 3d printing it.

## Purpose

We tried to make this design  durable, modular, easy to produce and adaptable to various types of hardware. Commercial Banding Wheels are usually made of cast iron. It's unfair to compare costs or quality. However, for basic tasks, this banding wheel can cost you 1/10 of a commercial one.

Going through this documentaion guide, we hope you enjoy printing, modifying the design and assembling this Banding Wheel. Any kind of feedback is always welcome. Use our [project's repository](https://gitlab.com/retalia/versa-wheel/) or the Printables platform get in contact.

# 1 - 3d Printing

In `printables/stl` folder you will find all .stl files to print. They are set for a 10mm system of threaded rods, nuts and bearings. You could get away with directly printing these .stl files if your hardware parts closely match those described in the bill of materials. We strongly advice though to first print the calibration part `printables/stl/calibration.stl`and test hardware fitting as described in the next section.

If you take the safest path, the typical 3d printing workflow involves opening `printables/setup.scad`, tweaking Customizer parameters there if needed, selecting different part using `PART` parameter, export to .stl and print the parts one by one. Follow along this manual and start with `calibration` part to test hardware fitting with bearing and screws. Next, move on to the rest of the parts.

Large scale modifications like using a different hardware sytem of 8 or 12mm is no different. Make sure you measure your hardware parts with caution and update all relevant parameters in `printables\setup.scad`. See the Capter [Size](#user-content-customizer-size) in _Appendix_ of this manual for more information on this.


## calibration.stl

This part helps you verify fitting of bearing and screws in the 3D printed parts:

<img src="photos/prova1.jpg" width = "300">

Fit the bearing in. Maybe the first layer needs to be sanded to fix [elephant foot](https://commons.wikimedia.org/wiki/File:3D_printing_calibration_elephant_foot.png). Make sure it has tight contact and its top surface levels with the calibration part.

<img src="photos/prova2.jpg" width = "300">
<img src="photos/prova4.jpg" width = "300">
<img src="photos/prova5.jpg" width = "300">

Fit the screw in the small opening. It should run freely through the hole and not tightly screw inside it:

<img src="photos/prova8.jpg" width = "300">
<img src="photos/prova7.jpg" width = "300">

See chapter [Printing Tolerances](#user-content-customizer-tolerances-ανοχές-εκτύπωσης) in _Appendix_ of this manual for troubleshooting.

## distancer.stl

Then, make a 3d print of 'distancer.stl'. This part should have a loose fit around the threaded rod and run on it easily, but with no tolerances. Print it with 100% infill slicer setting.

<img src="photos/distancer.jpg" width = "300">

## rotor.stl and stator.stl

Assuming that hardware matching verification with  'calibration.stl' part went well, move forward with the main parts. Render to .stl and print `stator` and `rotor` parts. They may look the same, but they have differences. Mark them, so you don't get them mixed up.

Slicing configuration:

  * infill = 20%
  * enable support
  * perimeters = 2

For any modifications in 3d printing parts, check [Appendix](#user-content-παράρτημα).

# 2 - Rotor Assembly

Rotor is the cone shaped 3d part with a hole on the base. This will become the upper, rotating part of the Banding Wheel. 

To assemble the rotor, put all the parts on the threaded rod as shown in the picture below. Make sure you have their order right: nut, bearing, small washer, distancer (3d print part), small washer. Leave the second bearing and nut for later:

<img src="photos/rotor1.jpg" width = "300"> 
<img src="photos/rotor2.jpg" width = "300"> 

Take care so the starting nut is level with the rod's face.

<img src="photos/rotor5.jpg" width = "300"> 
<img src="photos/rotor3.jpg" width = "300">
<img src="photos/rotor4.jpg" width = "300"> 

Pass the other side of the rod inside the rotor.

<img src="photos/rotor6.jpg" width = "300"> 

First pass the small washer, then the second bearing, the final nut and tighten by hand:

<img src="photos/rotor7.jpg" width = "300"> 
<img src="photos/rotor8.jpg" width = "300"> 

You should now tighten the assembled parts together properly. Use two additional nuts to make a _lock nut_ at the other end of the rod. Tighten them together with two wrenches.

<img src="photos/rotor9.jpg" width = "300"> 
<img src="photos/rotor10.jpg" width = "300"> 

Holding the _lock nut_ with one wrench (make sure you grip the right one), tighten the other one against the rotor.

<img src="photos/rotor11.jpg" width = "300"> 
<img src="photos/rotor12.jpg" width = "300"> 

Tighten well but do not overtighten. Remember, there is the plastic distancer part hanging in the middle of the rod that gets all the pressure. You will notice that the left side nut slowly gets inside the rotor. At the end, you should be able to rotate the rod but not untighten the nuts using bare hands.

<img src="photos/rotor13.jpg" width = "300"> 

Release the _lock nuts_ and remove them:

<img src="photos/rotor15.jpg" width = "300"> 

Assembled rotor:

<img src="photos/rotor16.jpg" width = "300"> 
<img src="photos/rotor17.jpg" width = "300"> 


# 3. Wood sheets and assembly

You need two wooded (preferably plywood) sheets for the Stator (base) and Rotor (rotating part) parts respectively. Start with a rectanguler shape and make a cross with two diagonals to spot the center.

Attach the stator to the wooded sheet. This is the base that will eventually get fixed on your workbench:

<img src="photos/wood1.jpg" width = "300"> 

Put a nut in the slot of the 3d printed part. Then, turn it over and put in on the center of the wooden sheet:

<img src="photos/wood2.jpg" width = "300"> 
<img src="photos/wood3.jpg" width = "300"> 

Push a screw to mark the positions of the holes. The 3d printed part should be in a fixed position until you mark all the holes.

<img src="photos/wood4.jpg" width = "300"> 
<img src="photos/wood5.jpg" width = "300"> 

Make holes using a drill (slightly smaller than the screw diameter). Some wood sheets, do not need drilling (plywood sheet will be fine without drilling).

<img src="photos/wood6.jpg" width = "300"> 
<img src="photos/wood7.jpg" width = "300"> 

Place the 3d printed part in the center and put the screws in the marked holes. Start with screws in opposite positions. Tighten so that the 3d printed part is snuggly attached to the wooden sheet.

<img src="photos/wood8.jpg" width = "300"> 
<img src="photos/wood9.jpg" width = "300"> 

You just made the base (_fixed part_)! Next comes the _rotating part_.

Follow the [same method](.) for the rotor to drill holes and attach to the second wooden sheet. As before, it is important to have the 3d printed part snuggly attached to the sheet:

<img src="photos/wood11.jpg" width = "300"> 

Back to the stator part, install a nut and the big washer as shown.

<img src="photos/wood12.jpg" width = "300"> 


Let the threaded rod pass through the Stator. Tighten or push it through until its threads catch up on the (embedded) nut at the other side. Keep tightening carefully until the rod reaches the wooden sheet. You'll feel some kind of resistance when this happens. If you cannot do this with bare hands use a pair of pliers and a piece of cloth to avoid damaging the threads of the rod:

<img src="photos/wood13.jpg" width = "300"> 

After tightening the rod in the stator part, tighten the last nut. Note, you 'll need perform these last steps once more, after disassembling (once).

<img src="photos/wood14.jpg" width = "300"> 

Here is how your banding wheel should look like at this point. In the photo it's fixed on a table with two C-clamps making it steady for the next move.

<img src="photos/wood15.jpg" width = "300"> 

You'll now need to mark a circle on the wooden sheet. Hold the marker as steady as possible and give a spin to the rotor by hand. As the top sheet rotates mark a circle with the marker.

<img src="photos/wood16.jpg" width = "300"> 
<img src="photos/wood17.jpg" width = "300"> 

After marking the circle, disassembly the stator. Release the last nut and the threaded rod just by doing the last moves in the opposite way.

Hand tight, you're almost done...

Now, you have to cut the wooden sheet of the Rotor. There are two ways to do this. Use a hand jigsaw to cut the sheet following the circle line. Or, mark tangent lines and then cut straight parts off using a hand saw. We took the second option by cutting off four corners resulting in an octagon shape as showed in the picture:

<img src="photos/wood18.jpg" width = "300">

Cut the corners off with a hand saw following your marked line. You'll notice in the picture that we didn't disassamble the stator. You don't need to suffer as we did.

<img src="photos/wood19.jpg" width = "300"> 
<img src="photos/wood20.jpg" width = "300"> 
<img src="photos/wood21.jpg" width = "300"> 

Having the wooden sheet cun in shape, assemble the stator again using last steps. Use a sandpaper to finish the edges.

<img src="photos/wood22.jpg" width = "300"> 

Congratulations!

<img src="photos/banding wheel.jpg" width = "300"> 


# Appendix

## Modifing 3d printed parts - Using OpenSCAD

In some cases, you will need to modify the 3d printed parts.

1. Hardware bearings and screw don't fit in the 'calibration' part
2. The materials are different from what this manual suggests
3. You need a bigger or smaller banding wheel

For these cases, there are source '.scad' files to modify and render  new '.stl' files. You will need OpenSCAD application for that. You can download it [here](http://openscad.org/downloads.html). 

Start OpenSCAD and open the file 'printables/setup.scad'. Make sure the window 'customizer' is runnig as you can see in it the following picture.

<img src="screenshots/openscad1.png" width = "600"> 

`customizer` is a complete guide to modify this design. It has five tabs as you can see in the screenshot above:

* Part - which 3d print part you need
* Size - Size of the apparatus
* Tolerances - 3d print tolerances
* Hardware - materials you have measured
* Advanced - for advanced editing

## Customizer - Part

Choose 'assembly' while making changes, so you can preview the complete design.

When you are ready with your new design, you will need to render new set of '.stl' files. Choose the file you will need, starting with 'calibration' to see if your design is good. Then, move on to 'distancer', 'stator', 'rotor'. After chosing the part you need, go to 'Design' --> 'Render' (F6). Wait to hear the ringing bell and choose 'File' --> 'Export' --> 'Export as stl' (F7).

<img src="screenshots/openscad2.png" width = "600"> 

## Customizer - Size of the apparatus

You can easily make modifications for a smaller or bigger banding wheel. Open 'Size' tab and give the dimentions you need. Before choosing dimentions keep in mind that:
* Maximum dimentions of Stator and Rotor ensure good adhension to the wood sheets.
* In big diameters you will need more screws.
* In big diameters you will need bigger Threaded Rod (and bearings, nuts, washers)

## Customizer - Tolerances - 3d print tolerances

'calibration' is a amall 3d printed part that has opening with exactly the same dimentions with the main parts. This part allows testing whether standard components (like bearing and screw) fit correctly in the printed parts. You can print a small part for the test with no waste of time and materials. This practice also helps cope with differences in 3d printer environments.

In customizer, open the tab 'Tolerances - 3d print tolerances' and increase or decrease following these rules:

* **Bearing diameter tolerance** = is added to the diameter of bearing
* **Bearing thickness tolerance** = is added to the width of bearing
* **Distancer diameter tolerance** = is added to the diameter of Threaded Rod *(only for distancer 3d printed part)*
* **Nut diameter tolerance** = is added to the diameter of the nut
* **Rod cone tolerance** = is added to the diameter of Threaded Rod 

<img src="screenshots/openscad3.png" width = "600"> 

If the tolerances are properly set, the parts will match with hardware of differente size (ex for other size of bearings). 

Common mistakes when you use `calibration` part:

* *The bearing doesn't fit*

  You have to first check if you have elephant foot. If your printer cannot face it, then you need to sand elephant foot. If it stil doesn't fit, then you should modify 'Bearing diameter tolerance'. Increase if by 0.30 and print `calibration` again.

* *The bearing is not fixed but it moves*

  You first need to check bearing's dimentions. If are right, then try reducing 'Bearing diameter tolerance' by 0.3.

* *The bearing fits fine, but is overhangs or is too submerged*
  
  Check bearings width first. Then, if it overhangs, increase 'Bearing thickness tolerance' by 0.3, or if it too submerged, reduce it by 0.3. Try again with `calibration`.

* *The screw is not correct*

  The screw has to be able to fit the `calibration` part without screwing. Measure screw's dimention. If it stil doesn't go through easily, increase 'Bolt diam' (customizers hardware tab) by 0.3 and print another round of `calibration`.


## Customizer - Hardware - Standard Components that You Have Measured

It is important to know the dimensions of the materials you are using, such as nuts, washers, bearings, and threaded rods. While standardized materials are available commercially, the design can work with any material as long as you know the exact measurements, taken with a caliper. In the customizer, open the tab 'Hardware - Materials You Have Measured' and input their dimensions.

#### Threaded Rod

  * outer diameter = 'Rod diam'

The outer diameter of the Threaded Rod includes the threading

#### Nuts

  * distance between opposite sides = 'Nut diam'
  * thickness = 'Nut thickness'

#### Bearings

  * outer diameter = 'Bearing diam'
  * thickness = 'Bearing thickness'

The thickness of the bearing should be measured on along the inner piece

#### Washers (only the small ones)

  * outer diameter = 'Washer diam'
  * thickness = 'Washer thickness'

NOTE: The large washer does not play a role in this section.

#### Wood Screws

  * outer diameter = 'Bolt diam'
  * head diameter of the screw = 'Bolt head diam'
  * if the screw is conical (milled), check = 'Bolt coned'
    
  * For screws shorter than the suggested ones, you can reduce the parameter Advanced/Bolt depth so that the length of the screw inside the wood + Bolt_depth equals the total length of the screw. If the screws are too large, you can simply cut them after installation.

Note, `outer diameter` includes the threading.

[TODO: The above list does not include the inner diameter. You don't need to measure it, but you need to check if it's correct by testing the nuts, washers, and bearings on the Threaded Rod.]

## Customizer - Advanced - For Advanced Users

#### Distancer Tension Margin

In the rotor system, the two nuts are tightened along with the two small washers on the distancer. In the 3d printed part of the rotor, the bearings sit on two feet, which must have a little space after tightening. This variable increases this space. As the variable increases, the vertical balance of the rotor increases, as the nut-washer-bearing-distancer system moves, touching one foot or the other.
