/*
    Versa Wheel 

    Printed parts parameters
    
    * Start with tweaking basic model parameters ('u_' prefixed).
    * Measure your hardware parts like nuts, washers etc. using a calibre and fill up hardware parameters ('h_' prefixed).
    * Update tolerance parameters ('_tolerance' suffixed).
    * Print the calibration part calibration.scad and make sure the hardware parts fit in it.
    
*/


/* [Part - Selected part] */
Part = "assembly"; // ["calibration", "distancer", "stator", "rotor", "assembly"]


/* [Size - Size of the assembly] */
// Total height
Assembly_height = 100; //[50:300]
// Diamater of the lower part - fixed part(purple)
Stator_diameter = 100; //[50:200]
// Diameter of the top part - moving part (green)
Rotor_diameter = 100; //[50:200]
// Hole count for support bolts
Hole_count = 5; //[3:20]
 
/* [Printing tolerances ] */
// increases the diameter of the bearing nest
Bearing_diameter_tolerance = 0.2; //[0:0.05:1]
//increases the thickness of the bearing nest
Bearing_thickness_tolerance = 0.6; //[0:0.1:1.5]
// increases the internal diameter of the distancer
Distancer_diameter_tolerance = 1.5; //[0.5:0.1:3]
// increases the nut nest
Nut_diameter_tolerance = 0;//[0.0:0.1:1]
// increases the hole run through by the stator rod
Rod_cone_tolerance = 0.4; //[0.0:0.1:1.5]



/* [Hardware - Measured Standared Materials] */
// Diameter of Threaded Rod (External)
Rod_diam = 9.8;
// Nut size i.e. distance of opposite planes
Nut_diam = 16.7;
//
Nut_thickness = 7.7;
// Bearing diameter (External)
Bearing_diam = 25.9;
//
Bearing_thickness = 7.93;
// Small Washer OD (=Outer Diameter)
Washer_diam = 19.52;
// Small Washer thickness
Washer_thickness = 2.15;
// Wood screw diameter
Bolt_diam = 3.65; 
// Wood screw head diameter
Bolt_head_diam = 7.64;
// Is this a wood screw with countersink head ?
Bolt_coned = true;


/* [Advanced] */
// Margin between distancer and rotor
Distancer_margin = 1;
// Tweak distancer length - sets the distance between bearings
Distancer_length_tolerance = 0.0; //[-2:0.1:2]
// Stator height % over total height
Height_point_division = 43; //[20:1:100]
// Space between rotor and stator as a percentage over total height. It should be more than double the height a nut.
Height_gap = 5; //[0:1:30]
// Flattening of cone tip
Cone_top_rim = 3; //[1:20]
// Height of cone base
Cone_bottom_rim = 3; //[1:5]
// Length of bolt inside the cone (subtract the part of the bolt that screws into the wood plate from from total bolt length)
Bolt_depth = 10; //[4:20]



module __Customizer_Limit__ () {}  // no more customizer parameters after this 



// ****************** Hardware parameters **********************
  
// bearing
h_bearing_d_outer = Bearing_diam; // outer bearing diameter
h_bearing_d_outer_tolerance = Bearing_diameter_tolerance; 
h_bearing_t = Bearing_thickness; // bearing thickness
h_bearing_t_tolerance = Bearing_thickness_tolerance; // +sth for the 3d-printing shrinkage

// nut
h_nut_t = Nut_thickness; // nut thickness
h_nut_t_tolerance = +1;

    
// washer
h_washer_t = Washer_thickness; // washer thickness
h_washer_outer_d = Washer_diam; // outer washer diameter

// threaded rod
h_rod_d = Rod_diam;

// bolt
h_bolt_d = Bolt_diam; // bolt diameter
h_bolt_head_d = Bolt_head_diam;
h_bolt_coned = Bolt_coned; // is bolt countersunk? set to true or false


// ******************* Basic parameters ******************* 

u_height = Assembly_height; // height of the whole system
u_top_d = Stator_diameter; // cone diameter at the top
u_bottom_d = Rotor_diameter; // cone diameter at the bottom


u_hub_bottom_height = (u_height - Height_gap-Nut_thickness*2.2)* (1-Height_point_division /100); //from total height subtrack the Height_gap and the nut thickness*2.2(2nut and a washer=2.2)
u_bottom_rim_width = Cone_top_rim; // width of the rim surrounding the bearing of the bottom part
u_hole_count = Hole_count; // number of flange holes that attach it to the plywood plate
u_rod_margin = 3; // margin between upper nut and disk that affects the length of the threaded rod sticking out

u_hub_top_height = (u_height - Height_gap-Nut_thickness*2.2)* (Height_point_division /100); //from total height subtrack the Height_gap and the nut thickness
u_center_hole_d = h_rod_d + Rod_cone_tolerance; // diameter of the hole at the center of the fixed flange. Set a bit wider so that it screws inside it. 

u_distancer_margin = Distancer_margin; // Margin from the distance and the cone hole. Only from one side.
distancer_d_inner_tolerance = Distancer_diameter_tolerance;

// ****************** Advanced parameters ********************

c_flange_hole_head_d = h_bolt_head_d + 1; // the hole a bit wider from the bolt's head, TODO - do we really need this or use calibration.stl part to determine it ?
c_flange_hole_d = h_bolt_d + 0.5; // Diameter of flange hole for bolts. A bit bigger than the bolt diameter to let it pass freely.


// Common & calculated parameters

c_distancer_outer_d = h_washer_outer_d;

c_bearing_d_outer_hole = h_bearing_d_outer + h_bearing_d_outer_tolerance; 
c_bearing_t = h_bearing_t + h_bearing_t_tolerance;
c_nut_t = h_nut_t + h_nut_t_tolerance;

c_flange_hole_depth = Bolt_depth; // height of the threaded bolt part in flange
c_flange_t = Cone_bottom_rim; // thickness of the flange from the end of the cone up to the disk



include <stator.scad>
include <rotor.scad>
include <calibration.scad>
include <distancer.scad>
include <assembly.scad>

if (Part == "stator") {
    color("violet",1)
    hub_top();
} else
if (Part == "rotor") {
    color("lime",1) 
    hub_bottom();
} else
if (Part == "calibration") {
    color("orangered",1) {
        prova_hub();
        prova_bolt(prova_offset=0);
    }
} else
if (Part == "distancer") {
    color("orangered",1) 
    distancer();
}
if (Part == "assembly") {
    assembly();
}