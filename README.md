# Versa Wheel

This project is about developing and building a tabletop wheel for ceramic use (commonly known as banding wheel). It can also be used for any kind of wheel system. This design is based on:

* Open source design with a Creative Commons Attribution 4.0 International License (CC BY 4.0)
* An easily modifiable construction for universal use with materials of any size (Universal Modular Design)
* Use of minimum standard and inexpensive materials, along with basic tools
* Utilizations of 3D printing technology to create complex and modular components

This project was initiated by retalia.org in June 2022.
