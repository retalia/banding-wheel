//include <setup.scad>
include <common.scad>

//use <stator.scad>
//use <rotor.scad>
//use <distancer.scad>


module assembly() {
    translate([0,0,Assembly_height]) rotate([0,180,0]){
    color("orangered",1.0) 
    translate([0,0,h_bearing_t+Nut_thickness+3]) distancer();

    %color("lime",0.4) hub_bottom();

    %color("violet",0.5) translate([0,0,u_height]) mirror([0,0,1]) hub_top();
        
    color("grey", 1.0) cylinder(d=h_rod_d + distancer_d_inner_tolerance, h=u_height);
    
    //The 4 nuts    
    color("lightgrey", 1.0) translate([0,0,0]) cylinder(d=2*1.26* c_nut_t, h=h_nut_t+ h_nut_t_tolerance, $fn=6);    
    color("lightgrey", 1.0) translate([0,0,u_hub_bottom_height]) cylinder(d=2*1.26* c_nut_t, h=h_nut_t+ h_nut_t_tolerance, $fn=6);    
    color("lightgrey", 1.0) translate([0,0,u_height- u_hub_top_height-(h_nut_t+ h_nut_t_tolerance)]) cylinder(d=2*1.26* c_nut_t, h=h_nut_t+ h_nut_t_tolerance, $fn=6);
    color("lightgrey", 1.0) translate([0,0,u_height-(h_nut_t+ h_nut_t_tolerance)]) cylinder(d=2*1.26* c_nut_t, h=h_nut_t+ h_nut_t_tolerance, $fn=6);        
    }
}