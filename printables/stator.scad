/*
 *  Hub Top
 *
 *  The uppar part of the system. It consists of a cone shaped flange that is fixed together with 
 *  the threaded rod. The rod runs through the body of the flange and screws lightly in the plastic it
 *  consists of. It terminates with a nut nested and fixed inside the top of the flange (the rotating
 *  one that gets attached to the plywood. From the other side another nut gets tied to the flange bottom
 *  (with a washer).
 */
 
//include <setup.scad>
use <common.scad>

module hub_top() {  
    difference() {
        flange(
            base_d = u_top_d,
            base_t = c_flange_t, // base thickness until the start of the actual cone
            flange_height = u_hub_top_height, // flange height in total
            top_d = u_bottom_rim_width*2 + h_bearing_d_outer,  //  upper side of the cone
            //center_d = h_bearing_d_outer, // center hole diameter
            hole_count = u_hole_count, // number of bolts on the flange
            hole_margin = 6, // margin from the edge of the hole until the flange perimeter
            hole_head_d = c_flange_hole_head_d,
            hole_depth = c_flange_hole_depth,
            hole_d = c_flange_hole_d,
            bolt_coned = h_bolt_coned
        );        
        translate([0,0,u_hub_top_height]) {                    
                    translate([0,0,-u_hub_top_height-1]) cylinder(h=u_hub_top_height+2, d=u_center_hole_d);
        }
        translate([0,0,-1]) cylinder(h=h_nut_t+h_nut_t_tolerance+1, d=Nut_diam*1.155+Nut_diameter_tolerance, $fn=6); // 1.155 to convert angle-to-angle to side-to-side
    }
}



//echo("hub-top height: ", u_hub_top_height);
//hub_top();

